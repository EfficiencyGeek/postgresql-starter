# https://hub.docker.com/_/postgres/?tab=description
#!/bin/bash
#set -e
set -x

export POSTGRES_USER="postgres"
export POSTGRES_DB="postgres"
export MYDB="pgstarter"
createdb() {
	# https://stackoverflow.com/questions/18389124/simulate-create-database-if-not-exists-for-postgresql
	psql -U postgres -tc "SELECT 1 FROM pg_database WHERE datname = '$MYDB'" | grep -q 1 || psql -U postgres -c "CREATE DATABASE $MYDB"
	
	cd ../shared/postgresqltutorialcom/
	./init-user-db.sh restoredvdrental
	
}

dropdb() {
	# useful if we want to repeat/recreate/reset this setup.
	psql -U postgres -c "drop database pgstarter"
#	docker exec -it mypg psql -U postgres -c "drop database pgstarter"
}

step1() {

	psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$MYDB" <<-EOSQL
		CREATE USER pgstarter with encrypted password 'pgstarter';
		GRANT ALL PRIVILEGES ON DATABASE pgstarter TO pgstarter;
		
		\connect pgstarter;

		-- create schema
		create schema cta;
		set search_path to cta;	
	EOSQL
}

step2() {
	export POSTGRES_USER="postgres"
	export POSTGRES_DB="postgres"

	psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
		CREATE USER pgstarter;
		CREATE DATABASE pgstarter;
		GRANT ALL PRIVILEGES ON DATABASE pgstarter TO pgstarter;
		
		\connect pgstarter;

		-- create schema
		create schema cta;
		set search_path to cta;	
	EOSQL
}

main() {
	STARTCMD=${1:-createdb} 
	echo `date` Started $0:$STARTCMD.
	
	${STARTCMD}
	#createdb
}

main $*

# end docker entrypoint
