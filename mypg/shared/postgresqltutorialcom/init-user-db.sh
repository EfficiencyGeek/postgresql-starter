# https://hub.docker.com/_/postgres/?tab=description
#!/bin/bash
#set -e
set -x

export POSTGRES_USER="postgres"
export POSTGRES_DB="postgres"
export MYDB="dvdrental"
createdb() {
	# https://stackoverflow.com/questions/18389124/simulate-create-database-if-not-exists-for-postgresql
	psql -v ON_ERROR_STOP=1  -U postgres -tc "SELECT count(1) FROM pg_database WHERE datname = '$MYDB'" | grep -q 1 || psql -U postgres <<-EOSQL
		CREATE database $MYDB;
		
		\l
	EOSQL
}

dropdb() {
set -x
	# useful if we want to repeat/recreate/reset this setup.
#	psql -U postgres -c "DROP database $MYDB;\l;"
#	docker exec -it mypg psql -U postgres -c "drop database  $MYDB"
	psql -v ON_ERROR_STOP=1 -U postgres -tc "SELECT count(1) FROM pg_database WHERE datname = '$MYDB'" | grep -q 0 || psql -U postgres <<-EOSQL
		drop database $MYDB;
		
		\l
	EOSQL
}

pulldb() {
set -x
	#psql -h localhost -U postgres $MYDB < pgex/pgex_backup.pgsql

	rm dvdrental.gz
	curl -o dvdrental.gz http://www.postgresqltutorial.com/wp-content/uploads/2017/10/dvdrental.zip 
	gzip -df dvdrental.gz
}

restoredb() {
set -x
	#psql -h localhost -U postgres $MYDB < pgex/pgex_backup.pgsql
	pg_restore -U postgres -d $MYDB dvdrental
}

restoredvdrental() {
	# lean towards assuming the entire set will work with repo files, assume to not pull from website.  use the dvdrental file in the repo.
	# pulldb
	dropdb
	createdb
	restoredb
}

#DBROLE="w3role"
DBUSER="user3" # user3 used in pgex/pgex_backup.sql
DBSCHEMA="exercises"
createuser() {

#	psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$MYDB" <<-EOSQL
	psql -v ON_ERROR_STOP=1 -U postgres -tc "SELECT count(1) FROM pg_roles WHERE rolname = '$DBUSER'" | grep -q 1 || psql -U postgres <<-EOSQL
		CREATE ROLE $DBUSER WITH LOGIN ENCRYPTED PASSWORD 'password';
		GRANT CONNECT ON DATABASE $MYDB TO $DBUSER;
		GRANT SELECT ON ALL TABLES IN SCHEMA public TO $DBUSER;

		\du;
		
--		CREATE USER $DBUSER;
--		GRANT ALL PRIVILEGES ON DATABASE $MYDB TO $DBUSER;
		
		\connect $MYDB;

		-- create schema
		create schema $DBSCHEMA;
		set search_path to $DBSCHEMA;	
		
		\dn;
	EOSQL
}

dropuser() {

#	psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$MYDB" <<-EOSQL
	psql -v ON_ERROR_STOP=1 -U postgres -tc "SELECT count(1) FROM pg_roles WHERE rolname = '$DBUSER'" | grep -q 0 || psql -U postgres <<-EOSQL
		DROP ROLE $DBUSER;
		\du;
	EOSQL
}

step2() {
	export POSTGRES_USER="postgres"
	export POSTGRES_DB="postgres"

	psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
		CREATE USER pgstarter;
		CREATE DATABASE pgstarter;
		GRANT ALL PRIVILEGES ON DATABASE pgstarter TO pgstarter;
		
		\connect pgstarter;

		-- create schema
		create schema cta;
		set search_path to cta;	
	EOSQL
}

create() {
#	createdb && createuser && restoredb
	test "$(createdb)" == '' && test "$(createuser)" == '' && test "$(restoredb)" == ''
}

destroy() {
	dropdb && dropuser
}

main() {
	STARTCMD=${1:-createdb} 
	echo `date` Started $0:$STARTCMD.
	
	${STARTCMD}
	retcode="$?"
	echo "retcode="$retcode
	#createdb

	echo `date` Ended $0:$STARTCMD.
}


main $*
