# PostgreSQL Starter

### Tutorials - Data Ready
| Tutorial Site              |                                                           Notes |
| -------------------------- | --------------------------------------------------------------- |
| [postgresqltutorial.com](http://www.postgresqltutorial.com/) | Run [pgweb](http://localhost:8083/) and skip to section 2 as the tutorial database, tables, and data are already created. |

### PostgreSQL SQL IDEs
| Runtime        |  IDE     |  Cool | Reference |
| ------------- | ----------- | ----------- | ---- |
| Browser      | [pgweb](http://localhost:8083/) | Focused on SQL queries, data analysis, application developer | http://sosedoff.github.io/pgweb/ |
| Browser      | [pgadmin4](http://localhost:8082/) | Focused on DBA, DevOps developer | https://hub.docker.com/r/dpage/pgadmin4/ |

### PostgreSQL as JSON API Services - PostgREST
| API / Link    |  Purpose |
| ------------- | ----------- |
| [Actors by Lastname](http://localhost:3000/actor_info?last_name=like.Be*) | List Actors where Last Name starts with Be |
| [PostgREST Documentation](http://postgrest.org) | official documentation |

### PostgreSQL Programming Examples
| Example Link        |  Notes    |
| ------------- | ----------|
| [Servlets example](servlets) | Servlets example |
| [Servlets example](jsp) | Servlets example |
| [WebSocket Examples](websocket/index.xhtml) | Websocket Example |

