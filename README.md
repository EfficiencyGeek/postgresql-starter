# postgresql-starter

The quick way to setup and get started with PostgreSQL.  http://localhost:8080

## Next:
1. PR to Master
2. Move to github
3. Clean up
4. Remove myhugo
5. Slides to accompany presentation.
   1. Why
   2. Demo
   3. How / Components
   4. References
6. Gif to Accompany Self Use.
7. Move to github.com
8. End of Release1
9. JSON Calls GUI
10. AdventureWorks
		1. Data - https://github.com/lorint/AdventureWorks-for-Postgres
		2. Lesson - https://sqlzoo.net/wiki/AdventureWorks
11. Switch to hugo instead of tomcat for simplicity, as we just need html, no app server stuff yet (https://hub.docker.com/r/publysher/hugo/dockerfile).
	1. https://themes.gohugo.io/hugo-theme-lean-launch-page/
12. Import data for Intro SQL for Data Science since it is based on PG (https://www.datacamp.com/courses/intro-to-sql-for-data-science) with a pg imdb .sql file.
	2. Load data https://assets.datacamp.com/production/course_1946/datasets/films.sql
	3. https://www.datacamp.com/courses/joining-data-in-postgresql
13. Have utility to check needed/exposed ports by compose file are available at host.

## Worklog:
### 3/11/2019
1. Added test-postgrest, test-pg to runme.sh
### 3/5/2019
1. Focus on Tutorial Aspect
1. Improve landing page to have a table of service (, try it, documentation.
1. Link to tutorial site. http://www.postgresqltutorial.com/
1. Used MDLink to create landing page. http://dynalon.github.io/mdwiki/#!index.md
### 2/12/2019
1. Added postgrest with links to try it and documentation.
1. Limited tomcat files checked in, to just webapps, avoiding the conf folder.
1. Fix tomcat, default page 404 error.

### 2/11/2019
1. Set starter webapp as default app, page.
	1. Set Starting Webapp - https://stackoverflow.com/questions/5638787/default-web-app-in-tomcat
1. Use shared host volume for tomcat conf, webapps folders. 
	1. Edit the jsps at runtime via the shared volumes.
1. Updated to Tomcat 9. 

### 2/6/2019
1. Added tomcat container to docker-compose, allowing for manager-gui, admin-gui access.
1. Added restoredocker runme.sh function to clear out images, volumes.
1. Call restoredvdrental() from docker-entrypoint
	1. Use docker-entrypoint
1. Add to Docker image as curl is needed to pull dvdrental.zip
	1. apt-get update
	1. apt-get install curl
### 2/3/2019
1. Load the DVDRental files to complete MMF1.  Use mypg/shared/w3resource/init-user-db.sh with restoredvdrental().

## Backlog
1. Finish up loading w3resource pgex db in mypg/shared/w3resource/init-user-db.sh
1. Create sql to create table based on https://raw.githack.com/ParfaitG/WORKSHOPS/master/2QPGCONF/SQL_Postgres_Data.html
	1. Bus_routes
	1. Bus_rides

## Features:
1. Starts *PostgreSQL*
	1. Checks that pgstarter db is created using Docker HEALTHCHECK.
1. Starts *PGWeb*
1. Starts *PgAdmin4*
1. Ensures services are started in sequence, using the *start_dependencies* image.

## Tutorial
1. http://www.postgresqltutorial.com/
	1. DB Data Files - https://github.com/EfficiencyGeek/dvdrental
	1. https://github.com/ajgreyling/dvdrental
1. https://www.w3resource.com/PostgreSQL/tutorial.php
1. https://github.com/AlisdairO/pgexercises

## Install
```
git clone git@bitbucket.org:EfficiencyGeek/postgresql-starter.git
cd postgresql-starter
```

## Try out
### Start the services
Open a command line window.
```
./runme.sh start
```
### Launch the starting page with relevant bookmarks
```
http://localhost:8080
```
### Test each service from the command line.
```
docker exec -it postgresql-starter_mypg_1 psql -U postgres -c"select version();"
docker exec -it postgresql-starter_mypgweb_1 wget http://localhost:8081
docker exec -it postgresql-starter_mypgadmin4_1 wget http://localhost:80
```
### To access PostgreSQL from the command line.
```
docker exec -it postgresql-starter_mypg_1  psql -U postgres
```
Type \q to quit psql.

### To quickly view the data in PostgreSQL, use PGWeb
```
http://localhost:8081
```
### For a more comprehensive, administrative centric view of PostgreSQL, use PGAdmin4.
```
http://localhost:8082
```
### Stop the services
Press ```Control-C``` into the command line window.

## Requires:
1. git (Git Bash)
1. bash (Git Bash)
1. docker
1. docker-compose

## Developed using:
1. Git Bash in Windows 10
1. bash ```bash --version
GNU bash, version 4.4.23(1)-release (x86_64-pc-msys)
Copyright (C) 2016 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>```
1. docker ```docker -v
Docker version 18.06.1-ce, build e68fc7a3. ```
1. docker-compose ```docker-compose -v
docker-compose version 1.22.0, build f46880f4.```
1. git ```git --version
git version 2.7.45. ```
1. dynalon.github.io/mdwiki/#!quickstart.md

