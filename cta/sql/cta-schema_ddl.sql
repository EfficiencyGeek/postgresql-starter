-- create db
create database pgstarter;
\connect pgstarter;

-- create schema
create schema cta;
set search_path to cta;