-- https://raw.githack.com/ParfaitG/WORKSHOPS/master/2QPGCONF/SQL_Postgres_Data.html
-- BUS DATA
CREATE TABLE bus_routes (
   id SERIAL,
   route_id INTEGER PRIMARY KEY,
   route_name VARCHAR(255)
);