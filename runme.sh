#/bin/bash
set -x

start() {
	docker-compose --file=docker-compose.yml up --build --renew-anon-volumes 
#	docker-compose --file=docker-compose.yml up --build --renew-anon-volumes
	#docker-compose --file=docker-compose1.yml up --remove-orphans --build --renew-anon-volumes
}

start1() {
	#docker-compose up --build
	docker-compose --file=docker-compose1.yml up --remove-orphans --build --renew-anon-volumes
}

stop() {
	docker-compose stop
}

down() {
	# rebuild the images, resets to like the first time the images are started. e.g. pgadmin4 forgets about the servers added already.
	docker-compose down
}

psql() {
	docker exec -it postgresql-starter_mypg_1  psql -U postgres
}

prunedocker() {
	# https://www.digitalocean.com/community/tutorials/how-to-remove-docker-images-containers-and-volumes
	docker system prune -a
}

resetdocker() {
	# https://www.digitalocean.com/community/tutorials/how-to-remove-docker-images-containers-and-volumes#removing-volumes
	
	# remove images
	# containers
	docker stop $(docker ps -a -q)
	docker rm $(docker ps -a -q)
	
#	docker stop postgresql-starter_mypg postgresql-starter_mypg_data
	docker stop mypg, postgresql-starter_start_dependencies_1, postgresql-starter_mypgweb_1, postgresql-starter_mypgadmin4_1
	docker rm mypg, postgresql-starter_start_dependencies_1, postgresql-starter_mypgweb_1, postgresql-starter_mypgadmin4_1

	# volumes
	docker volume rm -f postgresql-starter_mypg_data
	docker volume rm -f postgresql-starter_mypgadmin4_data

}

test1() {
	docker exec -it postgresql-starter_mypg_1 psql -U postgres -c"select * from pg_tables limit 3;"

	# use URL, ports for INSIDE the container
	docker exec -it postgresql-starter_mypgweb_1 wget http://localhost:8081
	docker exec -it postgresql-starter_mypgadmin4_1 wget http://localhost:80

	# use URL, ports for outside the container
	curl http://localhost:8083
	curl http://localhost:8082
}

test-postgrest() {
set -x
	# test db and postgrest
	curl http://localhost:3000/actor_info?last_name=like.Be*
}	

test-pg() {
	winpty docker exec -it mypg psql -U postgres -c"SELECT * FROM pg_database WHERE datname like 'pgs%';"
	
	# docker exec -it mypg bash  /docker-entrypoint-initdb.d/init-user-db.sh
}

test() {

	test-pg

	test-postgrest
}

mypg-bash() {
	winpty docker exec -it mypg bash
}

main() {
	CMD=${1:-start}
	$CMD
}

main $*
	
